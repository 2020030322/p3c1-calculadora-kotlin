package com.example.p03c1_calculadora_kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private lateinit var btnIngresar : Button
    private lateinit var btnSalir : Button
    private lateinit var txtUsuario : EditText
    private lateinit var txtPass : EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes();
        btnIngresar.setOnClickListener { ingresar() }
        btnSalir.setOnClickListener { salir() }
    }
    private fun iniciarComponentes(){
        btnIngresar = findViewById(R.id.btnIngresar);
        btnSalir = findViewById(R.id.btnSalir);
        txtUsuario = findViewById(R.id.txtUsuario);
        txtPass = findViewById(R.id.txtPass);
    }

    private fun ingresar(){
        var strUsuario : String
        var strPass : String

        //getresources en java
        strUsuario = applicationContext.resources.getString(R.string.usuario)
        strPass = applicationContext.resources.getString(R.string.pass)

        if(strUsuario.toString().equals(txtUsuario.text.toString()) && strPass.toString().equals(txtPass.text.toString())){
            //hacer paquete para envio de información
            var bundle = Bundle();
            bundle.putString("usuario", txtUsuario.text.toString())
            //hacer intent para llamar otra actividad
            //.class para java
            val intent = Intent(this@MainActivity, CalculadoraActivity::class.java);
            intent.putExtras(bundle)
            //iniciar la actividad esperando o no respuesta
            startActivity(intent)

        }else{
            Toast.makeText(this.applicationContext, "El usuario o contraseña son incorrectos.", Toast.LENGTH_SHORT).show()
        }
    }
    private fun salir(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Inicio");
        confirmar.setMessage("Desea salir?");
        confirmar.setPositiveButton("Confirmar"){
                dialogInterface, which->finish()
        }
        confirmar.setNegativeButton("Cancelar"){
                dialogInterface, which->
        }
        confirmar.show()
    }
}