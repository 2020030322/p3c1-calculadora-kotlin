package com.example.p03c1_calculadora_kotlin

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

private lateinit var btnSumar : Button;
private lateinit var btnRestar : Button;
private lateinit var btnMultiplicar : Button;
private lateinit var btnDividir : Button;
private lateinit var btnLimpiar : Button;
private lateinit var btnRegresar : Button;
private lateinit var lblUsuario : TextView;
private lateinit var lblResultado : TextView;
private lateinit var txtNum1 : EditText;
private lateinit var txtNum2 : EditText;

//declaración del objeto Calculadora
private var calculadora = Calculadora(0,0);

class CalculadoraActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)
        iniciarComponentes();
        //asignación de funciones a los botones (Onclick)
        btnSumar.setOnClickListener{ btnSumar() }
        btnRestar.setOnClickListener { btnRestar() }
        btnMultiplicar.setOnClickListener { btnMultiplicar() }
        btnDividir.setOnClickListener { btnDividir() }

        btnLimpiar.setOnClickListener { btnLimpiar() }
        btnRegresar.setOnClickListener { btnRegresar() }

        //asignar al lblUsuario el usuario (por medio del Main Activity)
        var datos = intent.extras
        var usuario = datos!!.getString("usuario")
        lblUsuario.setText(usuario.toString());

    }

    private fun iniciarComponentes(){
        //botones
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        btnDividir = findViewById(R.id.btnDividir);
        btnMultiplicar = findViewById(R.id.btnMultiplicar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        //labels
        lblUsuario = findViewById(R.id.lblUsuario);
        lblResultado = findViewById(R.id.lblResultado);
        //campos de texto
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);

    }
    //funciones de operaciones para botones (no asignados aún).
    fun btnSumar(){
        if(txtNum1.text.toString().equals("") || txtNum2.text.toString().equals("")){
            Toast.makeText(this.applicationContext, "Ingrese números para efectúar operación.", Toast.LENGTH_SHORT).show()
        }else{
            calculadora.num1 = txtNum1.text.toString().toInt();
            calculadora.num2 = txtNum2.text.toString().toInt();
            var total = calculadora.suma();
            lblResultado.text = "Resultado de la suma: " + total.toString() + ".";
        }
    }
    fun btnRestar(){
        if(txtNum1.text.toString().equals("") || txtNum2.text.toString().equals("")){
            Toast.makeText(this.applicationContext, "Ingrese números para efectúar operación.", Toast.LENGTH_SHORT).show()
        }else{
            calculadora.num1 = txtNum1.text.toString().toInt();
            calculadora.num2 = txtNum2.text.toString().toInt();

            var total = calculadora.resta();
            lblResultado.text = "Resultado de la resta: " + total.toString() + ".";
        }
    }
    fun btnMultiplicar(){
        if(txtNum1.text.toString().equals("") || txtNum2.text.toString().equals("")){
            Toast.makeText(this.applicationContext, "Ingrese números para efectúar operación.", Toast.LENGTH_SHORT).show()
        }else{
            calculadora.num1 = txtNum1.text.toString().toInt();
            calculadora.num2 = txtNum2.text.toString().toInt();

            var total = calculadora.multiplicacion();
            lblResultado.text = "Resultado de la multiplicación: " + total.toString() + ".";
        }
    }
    fun btnDividir(){
        if(txtNum1.text.toString().equals("") || txtNum2.text.toString().equals("")){
            Toast.makeText(this.applicationContext, "Ingrese números para efectúar operación.", Toast.LENGTH_SHORT).show()
        }else{
            calculadora.num1 = txtNum1.text.toString().toInt();
            calculadora.num2 = txtNum2.text.toString().toInt();

            var total = calculadora.division();
            lblResultado.text = "Resultado de la división: " + total.toString() + ".";
        }
    }
    fun btnLimpiar(){
        txtNum1.setText("");
        txtNum2.setText("");
        lblResultado.setText(":: Resultado ::");
    }
    fun btnRegresar(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora");
        confirmar.setMessage("Regresar al MainActivity");
        confirmar.setPositiveButton("Confirmar"){
                dialogInterface, which->finish()
        }
        confirmar.setNegativeButton("Cancelar"){
                dialogInterface, which->
        }
        confirmar.show()
    }
}